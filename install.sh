#!/bin/sh
ln -sf "$(pwd)"/.kshrc "$HOME"/.kshrc
ln -sf "$(pwd)"/.profile "$HOME"/.profile
ln -sf "$(pwd)"/.xinitrc "$HOME"/.xinitrc
ln -sf "$(pwd)"/.xprofile "$HOME"/.xprofile
ln -sf "$(pwd)"/.xsession "$HOME"/.xsession
ln -sf "$(pwd)"/.aliasrc "$HOME"/.aliasrc
ln -sf "$(pwd)"/.sxhkdrc "$HOME"/.config/sxhkd/sxhkdrc
ln -sf "$(pwd)"/.defaultprograms "$HOME"/.defaultprograms
ln -sf "$(pwd)"/.wallpapers "$HOME"/.wallpapers
ln -sf "$(pwd)"/.mkbm "$HOME"/.mkbm
ln -sf "$(pwd)"/.sfeedrc "$HOME"/.sfeed/sfeedrc
ln -sf "$(pwd)"/.rssmenu "$HOME"/.rssmenu
ln -sf "$(pwd)"/.swallow "$HOME"/.swallow
#for i in .*; do ln -sf "$(pwd)/${i}" "~/${i}"; done
